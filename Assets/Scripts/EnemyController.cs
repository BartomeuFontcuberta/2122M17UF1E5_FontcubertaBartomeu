﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Transform trans;
    public Rigidbody2D body;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.left);
        Debug.Log("Sript");
        if (hit)
        {
            Debug.Log(hit.collider.name);
            Debug.Log("Hit");
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                Debug.Log("Left");
                body.AddForce(Vector3.left * 2);
            }
        }
        hit = Physics2D.Raycast(transform.position, Vector2.right);
        if (hit)
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                Debug.Log("right");
                body.AddForce(Vector3.right * 2);
            }
        }
    }
}
