﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float speed;
    public Rigidbody2D bulletRB;
    // Start is called before the first frame update
    void Start()
    {
        //bulletRB.AddForce(Vector3.right * speed);
    }

    // Update is called once per frame
    void Update()
    {
        bulletRB.AddForce(Vector3.right * speed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if (Random.Range(0, 100) <= GameManager.Instance.spawner.GetComponent<PlatformSpawnController>().ProbabilitatHeart)
            {
                Instantiate(GameManager.Instance.spawner.GetComponent<PlatformSpawnController>().Heart, collision.gameObject.GetComponent<Transform>().position, Quaternion.identity);
            }
                Destroy(collision.gameObject);
            
            GameManager.Instance.KillEnemy();
        }else if (collision.gameObject.tag != "Player")
        {
            Destroy(this.gameObject);
        }
            
    }
}
