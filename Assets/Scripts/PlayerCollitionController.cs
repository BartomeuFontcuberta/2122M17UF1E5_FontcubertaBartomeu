﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollitionController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.InitPlayer(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Enemy")
        {
            Destroy(collision.gameObject);
            GameManager.Instance.PlayerLostLive();
        }
        else if (collision.gameObject.tag == "Heart")
        {
            Destroy(collision.gameObject);
            GameManager.Instance.PlayerGetLive();
        }
    }
}
