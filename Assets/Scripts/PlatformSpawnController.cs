﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawnController : MonoBehaviour
{
    private float positionY;
    private float positionBase;
    private float[] positionsX = {-2.22f,-1.11f,0f,1.11f,2.22f};
    public GameObject[] Platforms;
    public GameObject Enemy;
    public int ProbabilitatEnemy;
    public GameObject Heart;
    public int ProbabilitatHeart;

    private int posAnterior=2;
    private int posActual=2;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.InitPlatformSpawner(gameObject);
        positionY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y + 2.1f;
        positionBase = Camera.main.ScreenToWorldPoint(new Vector3(0,Screen.height, 0)).y;
        generateBase();
        while (positionBase > positionY - 1)
        {
            newPlatform();
            positionY += 2;
        }
    }

    // Update is called once per frame
    void Update()
    {
        positionBase = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y;
        if (positionBase > positionY - 1)
        {
            newPlatform();
            positionY += 2;
        }
    }
    private void newPlatform()
    {
        /*int posActual=2;
        do
        {
            posActual = Random.Range(0, positionsX.Length);
        } while (posActual == posAnterior);
        posAnterior = posActual;*/
        if (posAnterior == 0)
        {
            posActual = 1;
        }else if (posAnterior == 4)
        {
            posActual = 3;
        }
        else if(Random.Range(0,100)>50)
        {
            posActual++;
        }
        else
        {
            posActual--;
        }
        Instantiate(Platforms[Random.Range(0, Platforms.Length)], new Vector3(positionsX[posActual] , positionY,0), Quaternion.identity);
        generateExtra();
        posAnterior = posActual;
    }
    private void generateExtra()
    {
        if (Random.Range(0, 100) <= ProbabilitatHeart)
        {
            Instantiate(Heart, new Vector3(positionsX[posActual], positionY + 1, 0), Quaternion.identity);
        }else if (Random.Range(0, 100) <= ProbabilitatEnemy)
        {
            int direccio;
            if (posActual > posAnterior)
            {
                direccio = -1;
            }
            else
            {
                direccio = 1;
            }
            Instantiate(Enemy, new Vector3(positionsX[posActual] + ((positionsX[1] /3)*direccio), positionY+1, 0), Quaternion.identity);
        }
    }
    public void generateBase()
    {
        float posY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y+0.1f;
        Debug.Log(posY);
        foreach (float posX in positionsX)
        {
            Instantiate(Platforms[Random.Range(0, Platforms.Length)], new Vector3(posX, posY, 0), Quaternion.identity);
        }
    }
}
