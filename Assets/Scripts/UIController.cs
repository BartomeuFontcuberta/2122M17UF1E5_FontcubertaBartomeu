﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public Text Lives;
    public Text Distance;
    public Text Result;
    public Text EnemiesDead;
    public Image ResultPanel;
    public Image GamePanel;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.InitCanvas(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Lives.text = (GameManager.Instance.lives.ToString());
        Distance.text = ((int)GameManager.Instance.distance + " m");
    }
    public void ShowResult()
    {
        GamePanel.gameObject.SetActive(false);
        ResultPanel.gameObject.SetActive(true);
        Result.text = ((int)GameManager.Instance.distance + " m");
        EnemiesDead.text = (GameManager.Instance.enemiesDead + " kills");
    }
    public void reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
