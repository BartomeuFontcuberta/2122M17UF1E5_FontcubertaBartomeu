﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    public int lives=2;
    internal float distance = 0;
    internal int enemiesDead = 0;
    internal GameObject camara;
    internal GameObject canvas;
    internal GameObject player;
    internal GameObject spawner;
    private float posBase;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void PlayerDead()
    {
        Destroy(player);
        canvas.GetComponent<UIController>().ShowResult();
    }
    public void PlayerLostLive()
    {
        lives--;
        if (lives <= 0)
        {
            PlayerDead();
            return;
        }
        Transform pTrans = player.GetComponent<Transform>();
        float posY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y + 0.5f;
        pTrans.position = new Vector3(0, posY, 0);
        spawner.GetComponent<PlatformSpawnController>().generateBase();
    }
    public void PlayerGetLive()
    {
        if (lives <3)
        {
            lives++;
        }
    }
    public void KillEnemy()
    {
        enemiesDead++;
    }

    public void InitPlatformSpawner(GameObject obj)
    {
        spawner = obj;
    }
    public void InitPlayer(GameObject obj)
    {
        player = obj;
    }
    public void InitCanvas(GameObject obj)
    {
        canvas = obj;
    }
    public void InitCamera(GameObject obj)
    {
        camara = obj;
    }
}
