﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.InitCamera(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.player != null)
        {
            Vector3 player = GameManager.Instance.player.GetComponent<Transform>().position;

            if (Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height / 2, 0)).y < player.y)
            {

                Transform trans = gameObject.GetComponent<Transform>();
                GameManager.Instance.distance += Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.WorldToScreenPoint(player).y, 0)).y
                    - Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.WorldToScreenPoint(trans.position).y, 0)).y;
                trans.position = new Vector3(trans.position.x, Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.WorldToScreenPoint(player).y, 0)).y, trans.position.z);
            }
        }
    }
}
