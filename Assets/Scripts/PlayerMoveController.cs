﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveController : MonoBehaviour
{
    private Rigidbody2D playerRB;
    private Transform playerTransform;
    public float speed = 0.1f;
    public float jumpForce = 300f;
    public float numJumps;
    public GameObject Bullet;
    private bool isGrounded = true;
    private bool goRight = true;
    // Start is called before the first frame update
    void Start()
    {
        playerRB = gameObject.GetComponent<Rigidbody2D>();
        playerTransform = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
    }
    void Jump()
    {
        /*if (numJumps > 0)
        {
            isGrounded = false;
        }*/

        if (isGrounded)
        {
            if (Input.GetAxis("Jump")>0)
            {
                //Debug.Log("Jump!");
                playerRB.AddForce(Vector3.up * jumpForce);
                numJumps++;
                isGrounded = false;
            }
        }
        /*else
        {*/
            if (Input.GetAxis("Horizontal") > 0)
            {
                goRight = true;
                playerTransform.position = new Vector3(playerTransform.position.x + speed, playerTransform.position.y, playerTransform.position.z);
            }else if (Input.GetAxis("Horizontal") < 0)
            {
                goRight = false;
                playerTransform.position = new Vector3(playerTransform.position.x-speed,playerTransform.position.y,playerTransform.position.z);
            }
        /*}*/
        if (Input.GetButtonDown("Fire1"))
        {
            Debug.Log("Shoting");
            GameObject insBullet = Instantiate(Bullet, playerTransform.position, Quaternion.identity);
            if (goRight)
            {
                insBullet.GetComponent<Transform>().position = new Vector3(playerTransform.position.x+0.5f, playerTransform.position.y, playerTransform.position.z);
insBullet.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                insBullet.GetComponent<Transform>().position = new Vector3(playerTransform.position.x - 0.5f, playerTransform.position.y, playerTransform.position.z);
                insBullet.GetComponent<BulletController>().speed *= -1;
            }
            //

            //
            //(Camera.main.ScreenToWorldPoint(new Vector3(0f,0f,0f)).x)
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isGrounded = true;
            numJumps = 0;
        }
    }
}
