﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitsController : MonoBehaviour
{
    /*private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Platform"))
        {
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Player"))
        {
            GameManager.Instance.lives--;
            if (GameManager.Instance.lives <= 0)
            {
                Destroy(other.gameObject);
            }
        }
    }*/
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameManager.Instance.PlayerLostLive();
        }else
        {
            Destroy(collision.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
